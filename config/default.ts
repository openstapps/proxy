/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import {ConfigFile} from '../src/common';

const config: ConfigFile = {
  activeVersions: ['1\\.0\\.\\d+', '2\\.0\\.\\d+'],
  hiddenRoutes: ['/bulk'],
  logFormat: 'default',
  metrics: false,
  outdatedVersions: ['0\\.8\\.\\d+', '0\\.5\\.\\d+', '0\\.6\\.\\d+', '0\\.7\\.\\d+'],
  output: '/etc/nginx/http.d/default.conf',
  rateLimitAllowList: ['127.0.0.1/32'],
  sslFilePaths: {
    certificate: '/etc/nginx/certs/ssl.crt',
    certificateChain: '/etc/nginx/certs/chain.crt',
    certificateKey: '/etc/nginx/certs/ssl.key',
    dhparam: '/etc/nginx/certs/dhparam.pem',
  },
};

export default config;
