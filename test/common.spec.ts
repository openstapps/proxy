/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
// tslint:disable:no-implicit-dependencies
// tslint:disable:no-magic-numbers
// tslint:disable:completed-docs
// tslint:disable:prefer-function-over-method
// tslint:disable:newline-per-chained-call
import {suite, test} from '@testdeck/mocha';
import {Logger} from '@openstapps/logger';
import {expect} from 'chai';
import {mkdirSync, writeFileSync, unlinkSync, rmdirSync} from 'fs';
import { resolve } from 'path';
import { isFileType } from '../src/common';

process.on('unhandledRejection', async (error) => {
  await Logger.error(error);

  process.exit(1);
});


@suite
export class CommonSpec {

  @test
  async testSSLCert() {
    const testCertDir = resolve(__dirname, 'certs');
    mkdirSync(testCertDir);
    const notAnExptectedFileTypeFilePath = resolve(testCertDir, 'notAnExptectedFileType.txt');
    const anExptectedFileTypeFilePath = resolve(testCertDir, 'notARealCert.crt');
    writeFileSync(notAnExptectedFileTypeFilePath,'Test');
    writeFileSync(anExptectedFileTypeFilePath,'Test');

    expect(isFileType(notAnExptectedFileTypeFilePath,'crt')).to.equal(false)
    expect(isFileType(anExptectedFileTypeFilePath,'crt')).to.equal(true)

    unlinkSync(notAnExptectedFileTypeFilePath);
    unlinkSync(anExptectedFileTypeFilePath);
    rmdirSync(testCertDir);
  }
}
